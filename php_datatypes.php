                <?php
                // 8 data taypes in php


                    // 4 data types : scalar
                //1.Boolean
                $myTestVar = true;
                var_dump($myTestVar);
                echo "<br>";






                //2. Integer

                $myTestVar = 85;
                var_dump($myTestVar);

                echo "<br>";

                //2. Float

                $myTestVar = 42.3;
                var_dump($myTestVar);

                echo "<br>";

                //3. String

                $myTestVar = "This is string";
                var_dump($myTestVar);
                echo "<br>";


                //2 Data types are compound types

                //1. Array

                //1. indexed array:

                $person = array(
                    array("x",4,4.7),
                    array("y",5,4.6),
                    array("z",6,4.5)
                );

                //Associative Array:

                $age = array(

                    "kuddus" => 45,
                    "Jorina" => 30,
                    "Moynar ma" => 45,
                    "abul" => 66,
                );

                echo "<br>";
                echo $age["Jorina"];


                $person = array(

                    "kuddus" => 45,
                    "Jorina" => 30,
                    "Moynar ma" => 45,
                    "abul" => 66,
                );

                echo "<br>";
                echo $age["Jorina"];


